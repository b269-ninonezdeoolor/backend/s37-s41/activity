const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


// Route for creating a course
// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });


router.post("/create", auth.verify, (req, res) =>
{
	const data =
	{
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the courses
router.get("/allCourses", (req, res) =>
{
	courseController.getAllCourses(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the active courses
router.get("/allActiveCourses", (req, res) =>
{
	courseController.getAllActiveCourses(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the inactive courses
router.get("/allInactiveCourses", (req, res) =>
{
	courseController.getAllInactiveCourses(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving specific course
router.get("/:courseId", (req, res) =>
{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:courseId", (req, res) =>
{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// ACTIVITY
// MY SOLUTION
// Route for archiving a course
// router.patch("/:courseId/archive", auth.verify, (req, res) =>
// {
//     const archiveData =
//     {
//         course: req.body,
//         courseId: req.params.courseId,
// 		userId: auth.decode(req.headers.authorization)._id
//     }

//     courseController.archiveCourse(archiveData).then(resultFromController => res.send(resultFromController));
// });

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, (req, res) =>
{
    courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;