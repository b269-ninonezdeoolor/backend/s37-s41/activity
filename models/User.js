const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		// "true" values defines if the field is required or not
		// "First name is required" value is the message that will be printed out in our terminal when data is not present
		required : [true, "First name is required!"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required!"]
	},
	email : {
		type : String,
		required : [true, "Email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required!"]
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required!"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled!"
			}
		}
	]
});


// "module.exports" is a way for Node JS to treat the value as a package that can be used by other files
module.exports = mongoose.model("User", userSchema);