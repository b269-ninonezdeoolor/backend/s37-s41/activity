// JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
const jwt = require('jsonwebtoken');

/*
ANATOMY
	Header - type of token and signing algorithm
	Payload - contains the claims (id, email, isAdmin)
	Signature - generate by putting the encoded header, encoded payload and applying the algorithm in the header
*/

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without defined secret keyword
const secret = "CourseBookingAPI";


// Token Creation
module.exports.createAccessToken = (user) =>
{
	const data =
	{
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// Generates the token uding the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
};


// Token Verification
module.exports.verify = (req, res, next) =>
{
	let token = req.headers.authorization;

	if(typeof token !== "undefined")
	{
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) =>
		{
			if(err)
			{
				return res.send({auth: "failed"});
			}
			else
			{
				next();
			}
		});
	}
	else
	{
		return res.send({auth: "failed"});
	};
};


// Token Decryption
module.exports.decode = (token) =>
{
	if(typeof token !== "undefined")
	{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>
		{
			if(err)
			{
				return null;
			}
			else
			{
				return jwt.decode(token, {complete: true}).payload;
			};
		});
	}
	else
	{
		return null;
	};
};