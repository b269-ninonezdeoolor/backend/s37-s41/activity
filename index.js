// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// This allows us to use all the routes defined in the "userRoute.js" file
const userRoute = require("./routes/userRoute");
// This allows us to use all the routes defined in the "courseRoute.js" file
const courseRoute = require("./routes/courseRoute");


// Server
const app = express();


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
app.use("/users", userRoute);
// Allows all the user routes created in "courseRoute.js" file to use "/courses" as route (resources)
app.use("/courses", courseRoute);


// Database
mongoose.connect("mongodb+srv://ninonezdeoolor11:admin123@zuitt-bootcamp.sul2u9q.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log(`Now connected to cloud database.`));


// Server listening
// Will use the defined port number for the application whenever environment variable is available to used port 4000 if none is defined.
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`API is listening on port ${process.env.PORT || 4000}...`));