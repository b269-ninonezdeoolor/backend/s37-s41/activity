const Course = require("../models/Course");

// Create a new course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed
// 		if (error) {
// 			return false;
// 		// Course creation successful
// 		} else {
// 			return true;
// 		};
// 	});
// };


module.exports.addCourse = (data) =>
{
	// User is an admin
	if(data.isAdmin)
	{
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) =>
		{
			// Course creation successful
			if(error)
			{
				return false;
			// Course creation failed
			}
			else
			{
				return true;
			};
		});
	};
	// User is not an admin
	let message = Promise.resolve("User must be an ADMIN to access this!");
	return message.then((value) =>
	{
		return {value};
	});
};


// Retrieve ALL courses
module.exports.getAllCourses = () =>
{
	return Course.find({}).then(result =>
	{
		return result;
	});
};


// Retrieve ALL active courses
module.exports.getAllActiveCourses = () =>
{
	return Course.find({isActive:true}).then(result =>
	{
		return result;
	});
};

// Retrieve ALL inactive courses
module.exports.getAllInactiveCourses = () =>
{
	return Course.find({isActive:false}).then(result =>
	{
		return result;
	});
};


// Retrieve specific course
module.exports.getCourse = (reqParams) =>
{
	return Course.findById(reqParams.courseId).then(result =>
	{
		return result;
	});
};


// Update a course
module.exports.updateCourse = (reqParams, reqBody) =>
{
	let updatedCourse = 
	{
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	};

	// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>
	{
		if(error)
		{
			return false;
		}
		else
		{
			return true;
		};
	});
};


// ACTIVITY
// Archiving a course
// MY SOLUTION
// module.exports.archiveCourse = (archiveData) =>
// {
// 	if(!archiveData || archiveData._id)
// 	{
// 		let message = Promise.resolve("You need to login to perform this action!");
// 		message.then((value) =>
// 		{
// 			console.log(value);
// 		});
// 	}

// 	const updatedCourseStatus =
// 	{
// 		isActive: false
// 	};

// 	return Course.findByIdAndUpdate(archiveData.courseId, updatedCourseStatus).then((course, error) =>
// 	{
// 		if(error)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			return true;
// 		};
// 	});
// };

// Archiving a course
module.exports.archiveCourse = (reqParams) =>
{
	let updateActiveField =
	{
		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) =>
	{
		if(error)
		{
			return false;
		}
		else
		{
			return true;
		};
	});
};